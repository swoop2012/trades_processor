/*
https://ecomfe.github.io/echarts-examples/public/editor.html?c=custom-gantt-flight

https://ecomfe.github.io/echarts-examples/public/editor.html?c=candlestick-brush
    */
var HEIGHT_RATIO = 0.6;
var DIM_CATEGORY_INDEX = 0;
var DIM_TIME_ARRIVAL = 1;
var DIM_TIME_DEPARTURE = 2;
var _draggable;
var _draggingEl;
var _dropShadow;
var _draggingCursorOffset = [0, 0];
var _draggingTimeLength;
var _draggingRecord;
var _dropRecord;
var _rawData;








    myChart.on('mousedown', function (param) {
        if (!_draggable || !param || param.seriesIndex == null) {
            return;
        }

        // Drag start
        _draggingRecord = {
            dataIndex: param.dataIndex,
            categoryIndex: param.value[DIM_CATEGORY_INDEX],
            timeArrival: param.value[DIM_TIME_ARRIVAL],
            timeDeparture: param.value[DIM_TIME_DEPARTURE]
        };
        var style = {lineWidth: 2, fill: 'rgba(255,0,0,0.1)', stroke: 'rgba(255,0,0,0.8)', lineDash: [6, 3]};

        _draggingEl = addOrUpdateBar(_draggingEl, _draggingRecord, style, 100);
        _draggingCursorOffset = [
            _draggingEl.position[0] - param.event.offsetX,
            _draggingEl.position[1] - param.event.offsetY
        ];
        _draggingTimeLength = _draggingRecord.timeDeparture - _draggingRecord.timeArrival;
    });

    myChart.getZr().on('mousemove', function (event) {
        if (!_draggingEl) {
            return;
        }

        // Move _draggingEl.
        _draggingEl.attr('position', [
            _draggingCursorOffset[0] + event.offsetX,
            _draggingCursorOffset[1] + event.offsetY,
        ]);

        // Check droppable place.
        var xPixel = _draggingEl.shape.x + _draggingEl.position[0];
        var yPixel = _draggingEl.shape.y + _draggingEl.position[1];
        var cursorData = myChart.convertFromPixel('grid', [xPixel, yPixel]);
        if (cursorData) {
            // Make drop shadow and _dropRecord
            _dropRecord = {
                categoryIndex: Math.floor(cursorData[1]),
                timeArrival: cursorData[0],
                timeDeparture: cursorData[0] + _draggingTimeLength
            };
            var style = {fill: 'rgba(0,0,0,0.4)'};
            _dropShadow = addOrUpdateBar(_dropShadow, _dropRecord, style, 99);
        }
    });

    myChart.getZr().on('mouseup', function () {
        // Drop
        if (_draggingEl && _dropRecord) {

            updateRawData() && myChart.setOption({
                series: {
                    id: 'flightData',
                    data: _rawData.flight.data
                }
            });
        }
        dragRelease();
    });
    myChart.getZr().on('globalout', dragRelease);

    function dragRelease() {
        if (_draggingEl) {
            myChart.getZr().remove(_draggingEl);
            _draggingEl = null;
        }
        if (_dropShadow) {
            myChart.getZr().remove(_dropShadow);
            _dropShadow = null;
        }
        _dropRecord = _draggingRecord = null;
    }

    function addOrUpdateBar(el, itemData, style, z) {
        var pointArrival = myChart.convertToPixel('grid', [itemData.timeArrival, itemData.categoryIndex]);
        var pointDeparture = myChart.convertToPixel('grid', [itemData.timeDeparture, itemData.categoryIndex]);

        var barLength = pointDeparture[0] - pointArrival[0];
        var barHeight = Math.abs(
            myChart.convertToPixel('grid', [0, 0])[1] - myChart.convertToPixel('grid', [0, 1])[1]
        ) * HEIGHT_RATIO;

        if (!el) {
            el = new echarts.graphic.Rect({
                shape: {x: 0, y: 0, width: 0, height: 0},
                style: style,
                z: z
            });
            myChart.getZr().add(el);
        }
        el.attr({
            shape: {x: 0, y: 0, width: barLength, height: barHeight},
            position: [pointArrival[0], pointArrival[1] - barHeight]
        });
        return el;
    }

    // This is some business logic, don't care about it.
    function updateRawData() {
        var flightData = _rawData.flight.data;
        var movingItem = flightData[_draggingRecord.dataIndex];

        // Check conflict
        for (var i = 0; i < flightData.length; i++) {
            var dataItem = flightData[i];
            if (dataItem !== movingItem
                && _dropRecord.categoryIndex === dataItem[DIM_CATEGORY_INDEX]
                && _dropRecord.timeArrival < dataItem[DIM_TIME_DEPARTURE]
                && _dropRecord.timeDeparture > dataItem[DIM_TIME_ARRIVAL]
            ) {
                alert('Conflict! Find a free space to settle the bar!');
                return;
            }
        }

        // No conflict.
        movingItem[DIM_CATEGORY_INDEX] = _dropRecord.categoryIndex;
        movingItem[DIM_TIME_ARRIVAL] = _dropRecord.timeArrival;
        movingItem[DIM_TIME_DEPARTURE] = _dropRecord.timeDeparture;
        return true;
    }

