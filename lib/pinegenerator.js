module.exports = {
    timeFrame: 24 * 3600 * 1000,//hours
    setTimeFrame(timeFrame) {
        this.timeFrame = timeFrame * 3600 * 1000
    },
    getPines(rows) {
        let groups = this.getGroups(rows)
        let sets = this.getSets(groups)
        return this.getStr(sets)
    },

    getGroups(rows) {
        let index, key, row, groups = {}
        for (index in rows) {
            key = this.getKey(rows[index])
            row = rows[index]
            if (!(key in groups)) {
                groups[key] = []
            }
            groups[key].push(` ${row[1]} >= time and ${row[1]} <= time + chartResolutionMs ? \r\n ${row[3]}:`)
        }
        return groups
    },

    getSets(groups) {
        let sets = {0: [], 1: []}, values, isBuyer, key, index
        for (key in groups) {
            values = groups[key]
            isBuyer = key.indexOf('_1') > -1 ? 1 : 0
            for (index in values) {
                if (!(index in sets[isBuyer])) {
                    sets[isBuyer][index] = []
                }
                sets[isBuyer][index].push(values[index])
            }
        }
        return sets
    },

    getStr(sets) {
        let str = "//@version=3\r\n\
study(\"High volume\", overlay=true)\r\n\
chartResolution = interval\r\n\
if isdaily\r\n\
    chartResolution := 24*60*interval\r\n\
if isweekly\r\n\
    chartResolution := 24*60*7*interval\r\n\
if ismonthly\r\n\
    chartResolution := 24*60*30*interval\r\n\
chartResolutionMs = chartResolution * 60 * 1000\r\n";
        let isBuyer, group, key, values
        for (isBuyer in sets) {
            group = sets[isBuyer]
            for (key in group) {
                values = group[key]
                if (!values.length)
                    continue
                str += `\r\ndata${isBuyer}${key} = `;
                str += values.join('')
                str += `na\r\nplot(data${isBuyer}${key}, linewidth=4, color=${isBuyer ? '#265f43' : '#cec424'}, style=circles)`
            }
        }
        return str
    },

    getKey(row) {
        let increasedTimeFrame = this.timeFrame + 1;
        let from = (increasedTimeFrame) * Math.floor(row[1] / (increasedTimeFrame))
        let to = (increasedTimeFrame) * Math.floor(row[1] / (increasedTimeFrame)) + this.timeFrame
        return `${from}-${to}_${row[2] > 0 ? 1 : 0}`
    }
}




