The app has been written with usage of Electron, Node.js, Vue.js, Echarts 

The app is getting trades from the Bitfinex API. After getting the trades, this trades will be displayed on chart. 

This app has even smaller timeframes, compared to the usual data. It can be useful to look in the market action in the time of the increased volatility and volumes. 
Horizontal volumes can be built by clicking the according button and by selecting the time interval on chart.

You can also view the specific trades and filter them by the volume. Pine script generation for tradingview is available.

To install the dependencies use:
```
npm install
```

To run the app use:
```
npm run-script buildAndRun
```

