const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')
const helper = require('./helper')
const BFX = require('bitfinex-api-node')
helper.setBFX(BFX)
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let searchResults = {trades:[], stat:{}}
let stat = {maxPrice:0, minPrice:0, maxVol:0, minVol:0}

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow()
    mainWindow.maximize()
    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })

    mainWindow.webContents.on('new-window', function (event, url) {

        event.preventDefault();
        electron.shell.openExternal(url);
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
})

electron.ipcMain.on('candles-by-history', async (event, arg) => {
    if (arg.date_from !== '' && arg.date_to !== '') {
        let timeFrame = arg.time_frame
        searchResults = await helper.getTrades(arg, event)
        event.sender.send('candles-by-history-reply', helper.getCandles(searchResults.trades, timeFrame))

    }
})
electron.ipcMain.on('update-time-frame', async (event, arg) => {
    event.sender.send('update-time-frame-reply', helper.getCandles(searchResults.trades, arg))
})
electron.ipcMain.on('filter-trades', async (event, arg) => {
    if ((arg.from !== '' || arg.to !== '') && searchResults.trades.length) {
        event.sender.send('filter-trades-reply', helper.filterTrades(searchResults.trades, arg.from, arg.to))
    }
})
electron.ipcMain.on('volumes', async (event, arg) => {
    let moment = require('moment')
    if ((arg.from !== '' && arg.to !== '') && searchResults.trades.length) {
        let dateFrom = +moment.utc(arg.from,"DD/MM/YYYY HH:mm:ss");
        let dateTo = +moment.utc(arg.to ,"DD/MM/YYYY HH:mm:ss");
        event.sender.send('volumes-reply', helper.getVolumeProfile(searchResults, Math.min(dateFrom, dateTo), Math.max(dateFrom, dateTo) + arg.time_frame * 1000))
    }
})
electron.ipcMain.on('pines', async (event, arg) => {
    if ((arg.from !== '' || arg.to !== '') && searchResults.trades.length) {
        let pineGenerator = require('./lib/pinegenerator')
        pineGenerator.setTimeFrame(arg.time_frame)
        let trades = helper.filterTrades(searchResults.trades, arg.from, arg.to)
        event.sender.send('pines-reply', pineGenerator.getPines(trades))
    }
})



