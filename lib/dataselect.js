module.exports = {
    _draggingEl: null,
    _initPosition: [0, 0],
    renderer: null,
    init(chart) {
        const {ipcRenderer} = window.require('electron')
        this.renderer = ipcRenderer
        this.chart = chart
        this.echarts = require('echarts')
        this.clamp = require('clamp')
        let self = this
        chart.chart.getZr().on('mousedown', function (param) {
            if (!chart.chart._selectable || !param || ('target' in param && param.target && 'style' in param.target &&
                    param.target && param.target.style.hasOwnProperty('text'))) {
                return true;
            }
            let style = {lineWidth: 2, fill: 'rgba(255,0,0,0.3)', stroke: 'rgba(255,0,0,0.8)', lineDash: [6, 3]}
            self._initPosition = [param.offsetX, param.offsetY]
            self._draggingEl = self.addOrUpdateBar(self._draggingEl, style, 100)
            return true;
        });

        chart.chart.getZr().on('mousemove', function (event) {
            if (!self._draggingEl) {
                return true;
            }
            self._draggingEl.attr({
                'shape': {
                    width: Math.abs(event.offsetX - self._initPosition[0]),
                    height: Math.abs(event.offsetY - self._initPosition[1]),
                },
                position: [Math.min(event.offsetX, self._initPosition[0]), Math.min(event.offsetY, self._initPosition[1])]
            })
            return true;
        });
        chart.chart.getZr().on('mouseup', function (event) {
            return self.dragRelease(event)
        });
        chart.chart.getZr().on('globalout', function (event) {
            return self.dragRelease(event)
        });
    },

    dragRelease(event) {
        if (this._draggingEl) {
            if (event.offsetX !== this._initPosition[0]) {
                let dataset = this.chart.chart.getOption().dataset[0].source
                let maxIndex = dataset.length - 1
                let from = this.clamp(this.chart.chart.convertFromPixel({xAxisIndex: 1}, this._initPosition[0]), 0, maxIndex);
                let to = this.clamp(this.chart.chart.convertFromPixel({xAxisIndex: 1}, event.offsetX), 0 ,maxIndex);
                event.data = {from:dataset[from][0], to:dataset[to][0]}
                this.chart.$emit('volumes-selected',event)
                // this.chart.$emit('volumes-selected',{from:dataset[from][0], to:dataset[to][0]})
            }
            this.chart.chart.getZr().remove(this._draggingEl);
            this._draggingEl = null;
        }
        return true
    },

    addOrUpdateBar(el, style, z) {
        if (!el) {
            el = new this.echarts.graphic.Rect({
                shape: {x: 0, y: 0, width: 0, height: 0},
                style: style,
                z: z
            });
            this.chart.chart.getZr().add(el);
        }
        el.attr({
            shape: {x: 0, y: 0},
            position: [this._initPosition[0], this._initPosition[1]]
        });
        return el;
    }
}




