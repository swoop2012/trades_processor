module.exports = {
    setBFX(BFX) {
        this.bfx = new BFX()
    },
    getTimeout(ms, cb) {
        return new Promise(resolve => setTimeout(() => {
            if (typeof cb === 'function') {
                cb()
            }
            resolve()
        }, ms));
    },

    async getTrades(arg, event) {
        let searchResults = []
        let stats = {minPrice: 0, maxPrice: 0, minVol: 0, maxVol: 0}
        const rest = this.bfx.rest(2, {transform: false});
        let dateFrom = new Date(arg.date_from);
        let dateTo = new Date(arg.date_to);
        let currentDate = new Date();
        let startTs = dateFrom.getTime();
        let endTs = currentDate.getTime() < dateTo.getTime() ? currentDate.getTime() : dateTo.getTime();
        let lastTradeFound = 0;
        let reserveOffset = 30 * 1000
        let countProcessed = 0
        let lastId = 0
        let limit = 5000
        let oldTrades = []
        let volume
        while (true) {
            let skip = 0
            let trades = await rest.trades(`t${arg.pair.toUpperCase()}`, startTs, endTs + reserveOffset, limit, 1).catch((e) => {
                console.error(e.message)
                skip = 1
            })
            if (oldTrades.length > limit) {
                oldTrades = oldTrades.slice(oldTrades.length - limit - 1)
            }
            if (!skip) {
                if (trades.length === 1 && lastId === trades[0][0]) {
                    break
                }
                trades.some(trade => {
                    if (lastId === trade[0] || oldTrades.indexOf(trade[0]) > -1) {
                        return
                    }
                    if (trade[1] > endTs) {
                        lastTradeFound = 1
                        return true;
                    }
                    searchResults.push(trade)
                    if (stats.minPrice === 0) {
                        stats.minPrice = stats.maxPrice = trade[3]
                        stats.minVol = stats.maxVol = Math.abs(trade[2])
                    }
                    else {
                        volume = Math.abs(trade[2])
                        stats.minPrice = Math.min(stats.minPrice, trade[3])
                        stats.maxPrice = Math.max(stats.maxPrice, trade[3])
                        stats.minVol = Math.min(stats.minVol, volume)
                        stats.maxVol = Math.max(stats.maxVol, volume)
                    }
                    startTs = trade[1];
                    lastId = trade[0]
                    oldTrades.push(trade[0]);
                })
                countProcessed += trades.length;
                event.sender.send('candles-status', searchResults.length)
                if (lastTradeFound || trades.length < limit) {
                    break;
                }
                await this.getTimeout(2000)
            }
            else {
                await this.getTimeout(10000)
            }

        }
        return {trades: searchResults, stats: stats}
    },

    getCandles(trades, timeFrame) {
        let date = new Date()
        let dateFormat = require('dateformat')
        let offset = date.getTimezoneOffset() * 60 * 1000
        let candles = {}
        let format = 'dd/mm/yyyy HH:MM:ss';
        let tradeDate, candlesList = [], candle
        timeFrame = parseInt(timeFrame) * 1000;
        trades.forEach(trade => {
            date.setTime(trade[1] - (trade[1] % timeFrame) + offset)
            tradeDate = dateFormat(date, format)
            if (tradeDate in candles === false) {
                candles[tradeDate] = {
                    open: trade[3],
                    close: trade[3],
                    high: trade[3],
                    low: trade[3],
                    volume: Math.abs(trade[2] * trade[3])
                }
            }
            else {
                candles[tradeDate].close = trade[3];
                if (candles[tradeDate].high < trade[3]) {
                    candles[tradeDate].high = trade[3]
                }
                if (candles[tradeDate].low > trade[3]) {
                    candles[tradeDate].low = trade[3]
                }
                candles[tradeDate].volume += Math.abs(trade[2] * trade[3])

            }
        })

        for (let dateTime in candles) {
            candle = [dateTime, candles[dateTime].open, candles[dateTime].high, candles[dateTime].low,
                candles[dateTime].close, candles[dateTime].volume]
            candlesList.push(candle)
        }
        return candlesList;

    },
    filterTrades(searchResults, from, to) {
        from = parseInt(from);
        to = parseInt(to);
        let trades = [];
        let dateFormat = require('dateformat')
        let format = 'dd/mm/yyyy HH:MM:ss.l';
        searchResults.forEach(trade => {
            let volume = Math.abs(trade[2]);
            if ((volume >= from && volume <= to) ||
                (!from && volume <= to) ||
                (!to && volume >= from)) {
                trade.push(dateFormat(trade[1], format))
                trades.push(trade)
            }
        })
        return trades;
    },
    getVolumeProfile(searchResults, fromTs, toTs) {
        let volumes = {}
        let ts, price, volume,
            divider = 5, diff
        let dividerPrecision
        if (searchResults.trades.length) {
            divider = +((searchResults.stats.maxPrice - searchResults.stats.minPrice) / 40).toPrecision(2)
            dividerPrecision = this.precision(divider)
            searchResults.trades.forEach(trade => {
                ts = trade[1]
                diff = trade[3] * 1.0 % divider
                price = +(trade[3] - diff).toFixed(dividerPrecision)
                volume = Math.abs(trade[2]) * trade[3];
                if ((!fromTs && !toTs) || (ts >= fromTs && ts <= toTs)) {
                    if (price in volumes) {
                        volumes[price] += volume
                    }
                    else {
                        volumes[price] = volume
                    }
                }
            })
        }
        let volumesList = []
        for (price in volumes) {
            volumesList.push([parseFloat(price), +(parseFloat(price) + divider).toFixed(dividerPrecision), volumes[price]])
        }
        return volumesList
    },
    precision(a) {
        if (!isFinite(a)) return 0;
        var e = 1, p = 0;
        while (Math.round(a * e) / e !== a) {
            e *= 10;
            p++;
        }
        return p;
    }

}